﻿using Data.Modelos.Auxiliares;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Interfaces.Auxiliares
{
    public interface IC_ProvinciaRepo:ICRUD<C_Provincia>
    {
    }
}
