﻿using Data.Modelos.Principales;

namespace Data.Interfaces
{
    public interface IDireccionRepo : ICRUD<Direccion>
    {
    }
}
