﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface ICRUD <T> where T : class
    {
        ActionResult <T> Save(T Entity);
        Task<ActionResult<IList<T>>> GetAll();
        T GetById(int id);
        ActionResult Delete(T entity);
        ActionResult <T> InsertOrUpdate(int id, T Entity);

    }
}
