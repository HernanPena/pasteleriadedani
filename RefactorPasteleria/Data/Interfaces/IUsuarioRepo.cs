﻿using Data.Modelos.Principales;

namespace Data.Interfaces
{
    public interface IUsuarioRepo : ICRUD<Usuario>
    {
    }
}