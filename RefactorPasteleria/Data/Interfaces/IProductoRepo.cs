﻿using Data.Modelos.Principales;

namespace Data.Interfaces
{
    public interface IProductoRepo : ICRUD<Producto>
    {

    }
}
