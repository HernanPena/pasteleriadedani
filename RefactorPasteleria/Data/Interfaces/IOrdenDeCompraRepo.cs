﻿using Data.Modelos.Principales;

namespace Data.Interfaces
{
    public interface IOrdenDeCompraRepo : ICRUD<OrdenDeCompra>
    {

    }
}
