﻿using Data.Modelos.Principales;

namespace Data.Interfaces
{
    public interface IDetallePagoRepo : ICRUD<DetallePago>
    {
    }
}
