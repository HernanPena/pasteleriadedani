﻿using Data.Interfaces.Auxiliares;
using System;

namespace Data.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IUsuarioRepo Usuarios { get; }
        IProductoRepo Producto { get; }
        IPagoRepo Pago { get; }
        IOrdenDeCompraRepo OrdenDeCompra { get; }
        IDireccionRepo Direccion { get; }
        IDetallePagoRepo DetallePago { get; }
        IDetalleOrdenDeCompraRepo DetalleOrdeDeCompra { get; }
        ICarritoRepo Carrito { get; }
        IC_Tipo_DocumentoRepo TipoDocumento{ get; }
        IC_RolRepo Rol { get; }
        IC_ProvinciaRepo Provincia { get; }
        IC_Metodo_PagoRepo MetodoPago { get; }
        IC_LocalidadRepo Localidad { get; }
        IC_Estado_OrdenRepo EstadoOrden { get; }
        IC_CategoriaRepo Categoria { get; }

    }
}