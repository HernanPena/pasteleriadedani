﻿using Data.Interfaces;
using Data.Modelos.Principales;

namespace Data.Repositorios
{
    public class DireccionRepo : RepositorioGenerico<Direccion>, IDireccionRepo
    {
        public DireccionRepo(Contexto contexto) : base(contexto)
        {
        }
    }
}