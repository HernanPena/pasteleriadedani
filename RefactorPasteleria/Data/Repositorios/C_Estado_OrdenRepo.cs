﻿using Data.Interfaces.Auxiliares;
using Data.Modelos.Auxiliares;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositorios
{
    public class C_Estado_OrdenRepo : RepositorioGenerico<C_Estado_Orden>, IC_Estado_OrdenRepo
    {
        public C_Estado_OrdenRepo(Contexto contexto) : base(contexto)
        { 
        
        }
    }
}
