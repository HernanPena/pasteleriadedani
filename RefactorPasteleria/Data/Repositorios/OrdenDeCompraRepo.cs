﻿using Data.Interfaces;
using Data.Modelos.Principales;

namespace Data.Repositorios
{
    public class OrdenDeCompraRepo : RepositorioGenerico<OrdenDeCompra>, IOrdenDeCompraRepo
    {
        public OrdenDeCompraRepo(Contexto contexto) : base(contexto)
        {
        }
    }
}