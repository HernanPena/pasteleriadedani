﻿using Data.Interfaces;
using Data.Modelos.Principales;

namespace Data.Repositorios
{
    public class DetallePagoRepo : RepositorioGenerico<DetallePago>, IDetallePagoRepo
    {
        public DetallePagoRepo(Contexto contexto) : base(contexto)
        {
        }
    }
}