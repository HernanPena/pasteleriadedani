﻿using Data.Interfaces.Auxiliares;
using Data.Modelos.Auxiliares;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositorios
{
    public class C_TIpo_DocumentoRepo : RepositorioGenerico<C_Tipo_Documento>, IC_Tipo_DocumentoRepo
    {
        public C_TIpo_DocumentoRepo(Contexto contexto) : base(contexto)
        { 
        
        }
    }
}
