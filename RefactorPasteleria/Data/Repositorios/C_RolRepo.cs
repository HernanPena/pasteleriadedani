﻿using Data.Interfaces.Auxiliares;
using Data.Modelos.Auxiliares;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositorios
{
    public class C_RolRepo : RepositorioGenerico <C_Rol>, IC_RolRepo
    {
        public C_RolRepo(Contexto contexto) : base(contexto)
        {
        
        }
    }
}
