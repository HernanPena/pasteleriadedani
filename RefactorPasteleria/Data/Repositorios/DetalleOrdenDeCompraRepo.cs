﻿using Data.Interfaces;
using Data.Modelos.Principales;

namespace Data.Repositorios
{
    public class DetalleOrdenDeCompraRepo : RepositorioGenerico<DetalleOrdenDeCompra>, IDetalleOrdenDeCompraRepo
    {
        public DetalleOrdenDeCompraRepo(Contexto contexto) : base(contexto)
        {
        }
    }
}