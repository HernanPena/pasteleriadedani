﻿using Data.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.Repositorios
{
    public class RepositorioGenerico<T> : ICRUD<T> where T : class, IEntity
    {
        protected readonly Contexto _context;
        protected DbSet<T> _item;

        public RepositorioGenerico(Contexto contexto)
        {
            _context = contexto;
            _item = contexto.Set<T>();
        }

        public ActionResult Delete(T entity)
        {
            try
            {
                _context.Set<T>().Remove(entity);
                return new OkObjectResult("Eliminado con exito");
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult(e.Message);
            }
        }

        public async Task<ActionResult<IList<T>>> GetAll()
        {
            try
            {
                return await _context.Set<T>().ToListAsync();
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult(e.Message);
            }
        }

        public T GetById(int id)
        {
            try
            {
                return _context.Set<T>().Find(id);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public ActionResult<T> InsertOrUpdate(int id, T Entity)
        {
            try
            {
                var o = GetById(id);
                if (o == null)
                {
                    _context.Set<T>().Add(Entity);
                    _context.SaveChanges();
                    return (Entity);
                }
                else
                {
                    _context.Entry(Entity).State = EntityState.Modified;
                    _context.SaveChanges();
                    return (Entity);
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult(e.Message);
            }
        }

        public ActionResult<T> Save(T Entity)
        {
            try
            {
                _item.Add(Entity);
                _context.SaveChanges();
                return Entity;
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult(e.Message);
            }
        }
    }
}