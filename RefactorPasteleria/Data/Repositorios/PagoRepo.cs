﻿using Data.Interfaces;
using Data.Modelos.Principales;

namespace Data.Repositorios
{
    public class PagoRepo : RepositorioGenerico<Pago>, IPagoRepo
    {
        public PagoRepo(Contexto contexto) : base(contexto)
        {
        }
    }
}