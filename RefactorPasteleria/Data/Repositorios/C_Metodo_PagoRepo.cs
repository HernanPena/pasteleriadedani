﻿using Data.Interfaces.Auxiliares;
using Data.Modelos.Auxiliares;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositorios
{
    public class C_Metodo_PagoRepo :RepositorioGenerico<C_Metodo_Pago>,IC_Metodo_PagoRepo
    {
        public C_Metodo_PagoRepo(Contexto contexto) : base(contexto)
        { 
        }
    }
}
