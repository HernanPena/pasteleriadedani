﻿using Data.Interfaces;
using Data.Modelos.Principales;

namespace Data.Repositorios
{
    public class ProductoRepo : RepositorioGenerico<Producto>, IProductoRepo
    {
        public ProductoRepo(Contexto contexto) : base(contexto)
        {
        }
    }
}