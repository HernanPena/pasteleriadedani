﻿using Data.Interfaces.Auxiliares;
using Data.Modelos.Auxiliares;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositorios
{
    public class C_LocalidadRepo:RepositorioGenerico<C_Localidad>,IC_LocalidadRepo
    {
        public C_LocalidadRepo(Contexto contexto) : base(contexto)
        { 
        
        }
    }
}
