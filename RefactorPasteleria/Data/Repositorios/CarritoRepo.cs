﻿using Data.Interfaces;
using Data.Modelos.Principales;

namespace Data.Repositorios
{
    public class CarritoRepo : RepositorioGenerico<Carrito>, ICarritoRepo
    {
        public CarritoRepo(Contexto contexto) : base(contexto)
        {
        }
    }
}