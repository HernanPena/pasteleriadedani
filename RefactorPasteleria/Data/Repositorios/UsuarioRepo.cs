﻿using Data.Interfaces;
using Data.Modelos.Principales;

namespace Data.Repositorios
{
    public class UsuarioRepo : RepositorioGenerico<Usuario>, IUsuarioRepo
    {
        public UsuarioRepo(Contexto contexto) : base(contexto)
        {
        }
    }
}