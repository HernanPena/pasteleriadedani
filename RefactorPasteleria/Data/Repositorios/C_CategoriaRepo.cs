﻿using Data.Interfaces.Auxiliares;
using Data.Modelos.Auxiliares;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositorios
{
    public class C_CategoriaRepo:RepositorioGenerico<C_Categoria>,IC_CategoriaRepo
    {
        public C_CategoriaRepo(Contexto contexto) : base(contexto)
        { 
        
        }
    }
}
