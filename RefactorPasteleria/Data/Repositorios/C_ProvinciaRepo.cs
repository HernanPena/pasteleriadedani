﻿using Data.Interfaces.Auxiliares;
using Data.Modelos.Auxiliares;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositorios
{
    public class C_ProvinciaRepo :RepositorioGenerico<C_Provincia>, IC_ProvinciaRepo
    {
        public C_ProvinciaRepo(Contexto contexto) : base(contexto)
        { 
        
        }
    }
}
