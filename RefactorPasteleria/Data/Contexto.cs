﻿using Data.Modelos.Auxiliares;
using Data.Modelos.Principales;
using Microsoft.EntityFrameworkCore;

namespace Data
{
    public class Contexto : DbContext
    {
        public DbSet<C_Categoria> categorias { get; set; }
        public DbSet<C_Estado_Orden> estadosOrdenes { get; set; }
        public DbSet<C_Provincia> provincias { get; set; }
        public DbSet<C_Localidad> localidades { get; set; }
        public DbSet<C_Metodo_Pago> metodosPagos { get; set; }
        public DbSet<C_Rol> roles { get; set; }
        public DbSet<C_Tipo_Documento> tiposDocumentos { get; set; }

        public DbSet<Usuario> usuarios { get; set; }
        public DbSet<Producto> productos { get; set; }
        public DbSet<DetallePago> detallePagos { get; set; }
        public DbSet<Pago> pagos { get; set; }
        public DbSet<Carrito> carritos { get; set; }
        public DbSet<Direccion> direcciones { get; set; }
        public DbSet<DetalleOrdenDeCompra> detalleOrdenDeCompras { get; set; }
        public DbSet<OrdenDeCompra> ordenDeCompras { get; set; }

        public Contexto(DbContextOptions<Contexto> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Ignore<Entity>();
            base.OnModelCreating(modelBuilder);
        }
    }
}