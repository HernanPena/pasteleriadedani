﻿using Data.Interfaces;
using Data.Interfaces.Auxiliares;
using Data.Repositorios;

namespace Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly Contexto _contexto;

        public UnitOfWork(Contexto contexto)
        {
            _contexto = contexto;
            Usuarios = new UsuarioRepo(_contexto);
            Producto = new ProductoRepo(_contexto);
            Pago = new PagoRepo(_contexto);
            OrdenDeCompra = new OrdenDeCompraRepo(_contexto);
            Direccion = new DireccionRepo(_contexto);
            DetallePago = new DetallePagoRepo(_contexto);
            DetalleOrdeDeCompra = new DetalleOrdenDeCompraRepo(_contexto);
            Carrito = new CarritoRepo(_contexto);
            TipoDocumento = new C_TIpo_DocumentoRepo(_contexto);
            Rol = new C_RolRepo(_contexto);
            Provincia = new C_ProvinciaRepo(_contexto);
            MetodoPago = new C_Metodo_PagoRepo(_contexto);
            Localidad = new C_LocalidadRepo(_contexto);
            EstadoOrden = new C_Estado_OrdenRepo(_contexto);
            Categoria = new C_CategoriaRepo(_contexto);
        }

        public IUsuarioRepo Usuarios { get; set; }
        public IProductoRepo Producto { get; set; }
        public IPagoRepo Pago { get; set; }
        public IOrdenDeCompraRepo OrdenDeCompra { get; set; }
        public IDireccionRepo Direccion { get; set; }
        public IDetallePagoRepo DetallePago { get; set; }
        public IDetalleOrdenDeCompraRepo DetalleOrdeDeCompra { get; set; }
        public ICarritoRepo Carrito { get; set; }
        public IC_Tipo_DocumentoRepo TipoDocumento { get; set; }

        public IC_RolRepo Rol { get; set; }

        public IC_ProvinciaRepo Provincia { get; set; }

        public IC_Metodo_PagoRepo MetodoPago { get; set; }

        public IC_LocalidadRepo Localidad {get; set; }

        public IC_Estado_OrdenRepo EstadoOrden { get; set; }

        public IC_CategoriaRepo Categoria { get; set; }

        public void Dispose()
        {
            _contexto.Dispose();
        }
    }
}