﻿using System.ComponentModel.DataAnnotations;

namespace Data.Modelos.Auxiliares
{
    public class C_Rol : Entity
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere una descripcion del rol")]
        [MinLength(2, ErrorMessage = "Requiere como minimo 2 caracteres")]
        [MaxLength(20, ErrorMessage = "El maximo son 20 caracteres")]
        public string Rol { get; set; }
    }
}