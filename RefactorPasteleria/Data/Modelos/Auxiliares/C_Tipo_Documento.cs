﻿using System.ComponentModel.DataAnnotations;

namespace Data.Modelos.Auxiliares
{
    public class C_Tipo_Documento : Entity
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere especificar el tipo de documento")]
        [MinLength(2, ErrorMessage = "Requiere como minimo 2 caracteres")]
        [MaxLength(10, ErrorMessage = "El maximo son 10 caracteres")]
        public string Tipo_documento { get; set; }
    }
}