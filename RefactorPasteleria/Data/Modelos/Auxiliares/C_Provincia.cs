﻿using System.ComponentModel.DataAnnotations;

namespace Data.Modelos.Auxiliares
{
    public class C_Provincia : Entity
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Se necesita especificar la provincia")]
        public string Provincia { get; set; }
    }
}