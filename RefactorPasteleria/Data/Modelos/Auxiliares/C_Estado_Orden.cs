﻿using System.ComponentModel.DataAnnotations;

namespace Data.Modelos.Auxiliares
{
    public class C_Estado_Orden : Entity
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere descripcion del estado de la orden")]
        [MinLength(2, ErrorMessage = "Requiere como minimo 2 caracteres")]
        [MaxLength(20, ErrorMessage = "El maximo son 20 caracteres")]
        public string Estado { get; set; }
    }
}