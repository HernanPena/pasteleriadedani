﻿using System.ComponentModel.DataAnnotations;

namespace Data.Modelos.Auxiliares
{
    public class C_Metodo_Pago : Entity
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere escribir el nombre de la localidad")]
        [MinLength(2, ErrorMessage = "Requiere como minimo 2 caracteres")]
        [MaxLength(15, ErrorMessage = "El maximo son 15 caracteres")]
        public string Metodo { get; set; }

        public string Detalles_generales { get; set; }
        public string Detalle_transferencia { get; set; }
    }
}