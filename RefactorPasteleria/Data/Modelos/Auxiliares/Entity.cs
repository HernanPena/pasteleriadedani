﻿using Data.Interfaces;

namespace Data.Modelos.Auxiliares
{
    public class Entity : IEntity
    {
        public int Id { get; set; }
    }
}