﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Modelos.Auxiliares
{
    public class C_Localidad : Entity
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere escribir el nombre de la localidad")]
        [MinLength(2, ErrorMessage = "Requiere como minimo 2 caracteres")]
        [MaxLength(15, ErrorMessage = "El maximo son 15 caracteres")]
        public string Localidad { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere una provincia")]
        public int Id_provincia { get; set; }

        [Required]
        [ForeignKey("Id_provincia")]
        public C_Provincia Provincia { get; set; }
    }
}