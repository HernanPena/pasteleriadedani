﻿using Data.Modelos.Auxiliares;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Modelos.Principales
{
    public class Usuario : Entity
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere nombre para el usuario")]
        [MinLength(2, ErrorMessage = "Requiere como minimo 2 caracteres")]
        [MaxLength(20, ErrorMessage = "El maximo son 20 caracteres")]
        public string nombre { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere apellido para el usuario")]
        [MinLength(2, ErrorMessage = "Requiere como minimo 2 caracteres")]
        [MaxLength(20, ErrorMessage = "El maximo son 20 caracteres")]
        public string apellido { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere un correo electronico")]
        [DataType(DataType.EmailAddress)]
        [MaxLength(40, ErrorMessage = "El maximo son 40 caracteres")]
        public string email { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere un tipo de documento")]
        public int id_tipo_documento { get; set; }

        [ForeignKey("id_tipo_documento")]
        public C_Tipo_Documento tipoDocumento { get; set; }

        [Required]
        public string numero_documento { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Required]
        public string telefono { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "se necesita un password para el usuario")]
        //[RegularExpression(@"^(?=.*\d)(?=.*[a-z]).{6,12}$", ErrorMessage = "Contraseña NO valida")]
        [DataType(DataType.Password)]
        public string password { set; get; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "se necesita el rol del usuario")]
        public int rol_id { get; set; }

        [ForeignKey("rol_id")]
        public C_Rol rol { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime fecha_alta { set; get; }

        [Required]
        public bool activo { get; set; }
    }
}