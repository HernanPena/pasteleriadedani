﻿using Data.Modelos.Auxiliares;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Modelos.Principales
{
    public class Carrito : Entity
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere un usuario para continuar")]
        public int id_usuario { get; set; }

        [ForeignKey("id_usuario")]
        [Required]
        public Usuario usuario { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere un producto para continuar")]
        public int id_producto { get; set; }

        [ForeignKey("id_producto")]
        [Required]
        public Producto producto { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere la cantidad de producto para continuar")]
        [Range(1, 24, ErrorMessage = "Maximo 24 unidades")]
        public int cantidad { get; set; }

        [Required]
        public bool estado { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime fecha_alta { get; set; }
    }
}