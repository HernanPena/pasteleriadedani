﻿using Data.Modelos.Auxiliares;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Modelos.Principales
{
    public class Pago : Entity
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere una orden de compra asociada")]
        public int id_orden_compra { get; set; }

        [ForeignKey("id_orden_compra")]
        public OrdenDeCompra ordenDeCompra { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime fecha { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Es necesiario un precio para el producto")]
        [DataType(DataType.Currency)]
        [Range(0.0, double.MaxValue, ErrorMessage = "El valor es demasiado grande")]
        public double total { get; set; }
    }
}