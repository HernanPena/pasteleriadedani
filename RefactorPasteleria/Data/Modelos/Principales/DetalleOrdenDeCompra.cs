﻿using Data.Modelos.Auxiliares;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Modelos.Principales
{
    public class DetalleOrdenDeCompra : Entity
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere una orden de compra")]
        public int id_orden_compra { get; set; }

        [ForeignKey("id_orden_compra")]
        [Required]
        public OrdenDeCompra ordenDeCompra { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere codigo de producto")]
        public int id_producto { get; set; }

        [Required]
        [ForeignKey("id_producto")]
        public Producto producto { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Se necesita la cantidad del producto")]
        [Range(1, int.MaxValue, ErrorMessage = "El valor es demasiado grande")]
        public int cantidad { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Es necesiario un precio para el producto")]
        [DataType(DataType.Currency)]
        [Range(0.0, double.MaxValue, ErrorMessage = "El valor es demasiado grande")]
        public double precioUnitario { get; set; }
    }
}