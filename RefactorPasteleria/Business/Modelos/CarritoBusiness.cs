﻿using Business.Interfaces;
using Data.Interfaces;
using Data.Modelos.Principales;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Modelos
{
    public class CarritoBusiness : ICarritoBusiness
    {
        private readonly IUnitOfWork _unitOfWork;

        public CarritoBusiness(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ActionResult<Carrito> AgregarCarrito(Carrito c)
        {
            return _unitOfWork.Carrito.InsertOrUpdate(c.Id, c);
        }

        public ActionResult<Carrito> CarritoXid(int id)
        {
            return _unitOfWork.Carrito.GetById(id);
        }

        public ActionResult<Carrito> EliminarCarrito(int id)
        {
            var c = _unitOfWork.Carrito.GetById(id);
            if (c != null)
            {
                return _unitOfWork.Carrito.Delete(c);
            }
            else
            {
                return new BadRequestObjectResult("No se encontro un carrito");
            }
        }

        public Task<ActionResult<IList<Carrito>>> Listado()
        {
            return _unitOfWork.Carrito.GetAll();
        }
    }
}