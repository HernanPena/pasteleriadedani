﻿using Business.Interfaces;
using Data.Interfaces;
using Data.Modelos.Principales;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Modelos
{
    public class UsuarioBusiness : IUsuarioBusiness
    {
        private readonly IUnitOfWork _unitOfWork;

        public UsuarioBusiness(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ActionResult<Usuario> AcctualizarUsuario(Usuario u)
        {
            if (u != null)
            {
                return _unitOfWork.Usuarios.InsertOrUpdate(u.Id, u);
            }
            else
            {
                return new BadRequestObjectResult("No hay un usuario que cargar");
            }
        }

        public ActionResult<Usuario> BorrarUsuario(int id)
        {
            try
            {
                Usuario u = _unitOfWork.Usuarios.GetById(id);
                if (u != null)
                {
                    u.activo = false;
                    return _unitOfWork.Usuarios.InsertOrUpdate(id, u);
                }
                else
                {
                    return new BadRequestObjectResult("No existe usuario con ese Id");
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult(e.Message);
            }
        }

        public Task<ActionResult<IList<Usuario>>> Listado()
        {
            return _unitOfWork.Usuarios.GetAll();
        }

        ActionResult<Usuario> IUsuarioBusiness.UsuarioUnico(int id)
        {
            return _unitOfWork.Usuarios.GetById(id);
        }
    }
}