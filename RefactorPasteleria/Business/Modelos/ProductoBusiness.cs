﻿using Business.Interfaces;
using Data.Interfaces;
using Data.Modelos.Principales;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Modelos
{
    public class ProductoBusiness : IProductoBusiness
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProductoBusiness(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ActionResult<Producto> ActualizarOAgregarProducto(Producto p)
        {
            if (p != null)
            {
                return _unitOfWork.Producto.InsertOrUpdate(p.Id, p);
            }
            else
            {
                return new BadRequestObjectResult("No hay producto que cargar");
            }
        }

        public ActionResult<Producto> BorrarProducto(int id)
        {
            try
            {
                Producto p = _unitOfWork.Producto.GetById(id);
                if (p != null)
                {
                    p.activo = false;
                    return _unitOfWork.Producto.InsertOrUpdate(id, p);
                }
                else
                {
                    return new BadRequestObjectResult("No existe producto con ese Id");
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult(e.Message);
            }
        }

        public Task<ActionResult<IList<Producto>>> Listado()
        {
            return _unitOfWork.Producto.GetAll();
        }

        public ActionResult<Producto> ProductoXId(int id)
        {
            return _unitOfWork.Producto.GetById(id);
        }
    }
}