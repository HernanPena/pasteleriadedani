﻿using Business.Interfaces;
using Data.Interfaces;
using Data.Modelos.Principales;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Modelos
{
    public class OrdenDeCompraBusiness : IOrdenDeCompraBusiness
    {
        private readonly IUnitOfWork _unitOfWork;

        public OrdenDeCompraBusiness(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ActionResult<OrdenDeCompra> AgregarOrdenCompra(OrdenDeCompra OdC)
        {
            if (OdC != null)
            {
                return _unitOfWork.OrdenDeCompra.InsertOrUpdate(OdC.Id, OdC);
            }
            else
            {
                return new BadRequestObjectResult("No hay una orden de compra para cargar");
            }
        }

        public ActionResult<OrdenDeCompra> EliminarORdenCompra(int id)
        {
            var OdC = _unitOfWork.OrdenDeCompra.GetById(id);
            if (OdC != null)
            {
                return _unitOfWork.OrdenDeCompra.Delete(OdC);
            }
            else
            {
                return new BadRequestObjectResult("No se encontro Orden de compras");
            }
        }

        public Task<ActionResult<IList<OrdenDeCompra>>> Listado()
        {
            return _unitOfWork.OrdenDeCompra.GetAll();
        }

        public ActionResult<OrdenDeCompra> OrdenCompraXid(int id)
        {
            return _unitOfWork.OrdenDeCompra.GetById(id);
        }
    }
}