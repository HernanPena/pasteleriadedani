﻿using Business.Interfaces;
using Data.Interfaces;
using Data.Modelos.Principales;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Modelos
{
    public class PagoBusiness : IPagoBusiness
    {
        private readonly IUnitOfWork _unitOfWork;

        public PagoBusiness(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ActionResult<Pago> AgregarPago(Pago p)
        {
            if (p != null)
            {
                return _unitOfWork.Pago.InsertOrUpdate(p.Id, p);
            }
            else
            {
                return new BadRequestObjectResult("No hay pago quea gregar");
            }
        }

        public ActionResult<Pago> EliminarPago(int id)
        {
            var p = _unitOfWork.Pago.GetById(id);
            if (p != null)
            {
                return _unitOfWork.Pago.Delete(p);
            }
            else
            {
                return new BadRequestObjectResult("No se encuentra el id del pago");
            }
        }

        public ActionResult<Pago> PagoXId(int id)
        {
            return _unitOfWork.Pago.GetById(id);
        }

        Task<ActionResult<IList<Pago>>> IPagoBusiness.Listado()
        {
            return _unitOfWork.Pago.GetAll();
        }
    }
}