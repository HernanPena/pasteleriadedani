﻿using Business.Interfaces.Auxiliares;
using Data.Interfaces;
using Data.Modelos.Auxiliares;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Modelos.Auxiliares
{
    public class C_Provincia_Business : IC_Provincia_Business
    {
        private readonly IUnitOfWork _unitofwork;
        public C_Provincia_Business(IUnitOfWork unitOfWork)
        {
            _unitofwork = unitOfWork;
        }

        public ActionResult<C_Provincia> AgregarModificarProvincia(C_Provincia provincia)
        {
            return _unitofwork.Provincia.InsertOrUpdate(provincia.Id, provincia);
        }

        public ActionResult<C_Provincia> EliminarProvincia(int id)
        {
            var prov = _unitofwork.Provincia.GetById(id);
            if (prov != null)
            {
                return _unitofwork.Provincia.Delete(prov);
            }
            else
            {
                return new BadRequestObjectResult("No se encuentra provicia que eliminar");
            }
        }

        public Task<ActionResult<IList<C_Provincia>>> Listado()
        {
            return _unitofwork.Provincia.GetAll();
        }

        public ActionResult<C_Provincia> ProvinciaXid(int id)
        {
            return _unitofwork.Provincia.GetById(id);
        }
    }
}
