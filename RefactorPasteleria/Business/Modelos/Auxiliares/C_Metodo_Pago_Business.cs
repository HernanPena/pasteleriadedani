﻿using Business.Interfaces.Auxiliares;
using Data.Interfaces;
using Data.Modelos.Auxiliares;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Modelos.Auxiliares
{
    public class C_Metodo_Pago_Business:IC_Metodo_Pago_Business
    {
        private readonly IUnitOfWork _unitofwork;
        public C_Metodo_Pago_Business(IUnitOfWork unitofwork)
        {
            _unitofwork = unitofwork;
        }

        public ActionResult<C_Metodo_Pago> AgregarModificarMetodoPago(C_Metodo_Pago metodoPago)
        {
            return _unitofwork.MetodoPago.InsertOrUpdate(metodoPago.Id, metodoPago);
        }

        public ActionResult<C_Metodo_Pago> EliminarMetodoPago(int id)
        {
            var met = _unitofwork.MetodoPago.GetById(id);
            if (met != null)
            {
                return _unitofwork.MetodoPago.Delete(met);
            }
            else
            {
                return new BadRequestObjectResult("no se encuentra metodo de pago para eliminar");
            }
        }

        public Task<ActionResult<IList<C_Metodo_Pago>>> Listado()
        {
            return _unitofwork.MetodoPago.GetAll();
        }

        public ActionResult<C_Metodo_Pago> MetodoPagoXid(int id)
        {
            return _unitofwork.MetodoPago.GetById(id);
        }
    }
}
