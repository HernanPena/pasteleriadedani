﻿using Business.Interfaces.Auxiliares;
using Data.Interfaces;
using Data.Modelos.Auxiliares;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Modelos.Auxiliares
{
    public class C_Tipo_Documento_Business : IC_Tipo_Documento_Business
    {
        private readonly IUnitOfWork _unitOfWork;
        public C_Tipo_Documento_Business(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ActionResult<C_Tipo_Documento> AgregarTipoDoc(C_Tipo_Documento TIpoDoc)
        {
            return _unitOfWork.TipoDocumento.Save(TIpoDoc);
        }

        public ActionResult<C_Tipo_Documento> EliminarTipoDoc(int id)
        {
            var tipodoc = _unitOfWork.TipoDocumento.GetById(id);
            if (tipodoc != null)
            {
                return _unitOfWork.TipoDocumento.Delete(tipodoc);
            }
            else
            {
                return new BadRequestObjectResult("No se encuentra el tipo de documento");
            }
        }

        public Task<ActionResult<IList<C_Tipo_Documento>>> Listado()
        {
            return _unitOfWork.TipoDocumento.GetAll();
        }

        public ActionResult<C_Tipo_Documento> TipoDocXid(int id)
        {
            return _unitOfWork.TipoDocumento.GetById(id);
        }
    }
}
