﻿using Business.Interfaces.Auxiliares;
using Data.Interfaces;
using Data.Modelos.Auxiliares;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Modelos.Auxiliares
{
    public class C_Categoria_Business:IC_Categoria_Business
    {
        private readonly IUnitOfWork _unitofwork;
        public C_Categoria_Business(IUnitOfWork unitofwork)
        {
            _unitofwork = unitofwork;
        }

        public ActionResult<C_Categoria> AgregarModificarCategoria(C_Categoria categoria)
        {
            return _unitofwork.Categoria.InsertOrUpdate(categoria.Id, categoria);
        }

        public ActionResult<C_Categoria> CategoriaXid(int id)
        {
            return _unitofwork.Categoria.GetById(id);
        }

        public ActionResult<C_Categoria> EliminarCategoria(int id)
        {
            var cat = _unitofwork.Categoria.GetById(id);
            if (cat != null)
            {
                return _unitofwork.Categoria.Delete(cat);
            }
            else
            {
                return new BadRequestObjectResult("No se encuentra categoria que eliminar");
            }
        }

        public Task<ActionResult<IList<C_Categoria>>> Listado()
        {
            return _unitofwork.Categoria.GetAll();
        }
    }
}
