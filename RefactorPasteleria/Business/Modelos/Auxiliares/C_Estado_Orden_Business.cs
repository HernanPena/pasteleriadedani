﻿using Business.Interfaces.Auxiliares;
using Data.Interfaces;
using Data.Modelos.Auxiliares;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Modelos.Auxiliares
{
    public class C_Estado_Orden_Business:IC_Estado_Orden_Business
    {
        private readonly IUnitOfWork _unitofwork;
        public C_Estado_Orden_Business(IUnitOfWork unitofwork)
        {
            _unitofwork = unitofwork;
        }

        public ActionResult<C_Estado_Orden> AgregarModificarEstadoOrden(C_Estado_Orden estadoOrden)
        {
            return _unitofwork.EstadoOrden.InsertOrUpdate(estadoOrden.Id, estadoOrden);
        }

        public ActionResult<C_Estado_Orden> EliminarEstadoOrden(int id)
        {
            var est = _unitofwork.EstadoOrden.GetById(id);
            if (est != null)
            {
                return _unitofwork.EstadoOrden.Delete(est);
            }
            else
            {
                return new BadRequestObjectResult("No hay estado de orden para eliminar");
            }
        }

        public ActionResult<C_Estado_Orden> EstadoOrdenXid(int id)
        {
            return _unitofwork.EstadoOrden.GetById(id);
        }

        public Task<ActionResult<IList<C_Estado_Orden>>> Listado()
        {
            return _unitofwork.EstadoOrden.GetAll();
        }
    }
}
