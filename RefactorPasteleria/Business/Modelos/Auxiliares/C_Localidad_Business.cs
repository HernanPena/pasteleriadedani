﻿using Business.Interfaces.Auxiliares;
using Data.Interfaces;
using Data.Modelos.Auxiliares;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Modelos.Auxiliares
{
    public class C_Localidad_Business : IC_Localidad_Business
    {
        private readonly IUnitOfWork _unitofwork;

        public C_Localidad_Business(IUnitOfWork unitofwork)
        {
            _unitofwork = unitofwork;
        }

        public ActionResult<C_Localidad> AgregarModificarLocalidad(C_Localidad localidad)
        {
            return _unitofwork.Localidad.InsertOrUpdate(localidad.Id, localidad);
        }

        public ActionResult<C_Localidad> EliminarLocalidad(int id)
        {
            var loc = _unitofwork.Localidad.GetById(id);
            if (loc != null)
            {
                return _unitofwork.Localidad.Delete(loc);
            }
            else
            {
                return new BadRequestObjectResult("No se encuentra localidad para eliminar");
            }
        }

        public Task<ActionResult<IList<C_Localidad>>> Listado()
        {
            return _unitofwork.Localidad.GetAll();
        }

        public ActionResult<C_Localidad> LocalidadXid(int id)
        {
            return _unitofwork.Localidad.GetById(id);
        }
    }
}