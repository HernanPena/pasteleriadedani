﻿using Business.Interfaces.Auxiliares;
using Data.Interfaces;
using Data.Modelos.Auxiliares;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Modelos.Auxiliares
{
    public class C_Rol_Business : IC_Rol_Business
    {
        private readonly IUnitOfWork _unitOfWork;
        public C_Rol_Business(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ActionResult<C_Rol> AgregarOmodificarRol(C_Rol Rol)
        {
            return _unitOfWork.Rol.InsertOrUpdate(Rol.Id, Rol);
        }

        public ActionResult<C_Rol> EliminarRol(int id)
        {
            var rol = _unitOfWork.Rol.GetById(id);
            if (rol != null)
            {
                return _unitOfWork.Rol.Delete(rol);
            }
            else
            {
                return new BadRequestObjectResult("No existe el rol para eliminar");
            }
        }

        public Task<ActionResult<IList<C_Rol>>> Listado()
        {
            return _unitOfWork.Rol.GetAll();
        }

        public ActionResult<C_Rol> RolXid(int id)
        {
            return _unitOfWork.Rol.GetById(id);
        }
    }
}
