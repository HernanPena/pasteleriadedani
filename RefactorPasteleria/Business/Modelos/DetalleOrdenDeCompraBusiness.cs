﻿using Business.Interfaces;
using Data.Interfaces;
using Data.Modelos.Principales;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Modelos
{
    public class DetalleOrdenDeCompraBusiness : IDetalleOrdenDeCompraBusiness
    {
        private readonly IUnitOfWork _unitOfWork;

        public DetalleOrdenDeCompraBusiness(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ActionResult<DetalleOrdenDeCompra> AgregarDetalleOrdenDeCompra(DetalleOrdenDeCompra dodc)
        {
            return _unitOfWork.DetalleOrdeDeCompra.InsertOrUpdate(dodc.Id, dodc);
        }

        public ActionResult<DetalleOrdenDeCompra> DetalleOrdenDeCompraXid(int id)
        {
            return _unitOfWork.DetalleOrdeDeCompra.GetById(id);
        }

        public ActionResult<DetalleOrdenDeCompra> EliminarDetalleOrdenDeCompra(int id)
        {
            var dodc = _unitOfWork.DetalleOrdeDeCompra.GetById(id);
            if (dodc != null)
            {
                return _unitOfWork.DetalleOrdeDeCompra.Delete(dodc);
            }
            else
            {
                return new BadRequestObjectResult("no se encuentra detalle de orden de compra");
            }
        }

        public Task<ActionResult<IList<DetalleOrdenDeCompra>>> Listado()
        {
            return _unitOfWork.DetalleOrdeDeCompra.GetAll();
        }
    }
}