﻿using Business.Interfaces;
using Data.Interfaces;
using Data.Modelos.Principales;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Modelos
{
    public class DetallePagoBusiness : IDetallePagoBusiness
    {
        private readonly IUnitOfWork _unitOfWork;

        public DetallePagoBusiness(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ActionResult<DetallePago> AgregarDetallePago(DetallePago dp)
        {
            return _unitOfWork.DetallePago.InsertOrUpdate(dp.Id, dp);
        }

        public ActionResult<DetallePago> EliminarDetallePago(int id)
        {
            var dp = _unitOfWork.DetallePago.GetById(id);
            if (dp != null)
            {
                return _unitOfWork.DetallePago.Delete(dp);
            }
            else
            {
                return new BadRequestObjectResult("No se encuentra detalle de pago");
            }
        }

        public Task<ActionResult<IList<DetallePago>>> Listado()
        {
            return _unitOfWork.DetallePago.GetAll();
        }

        public ActionResult<DetallePago> PagoXid(int id)
        {
            return _unitOfWork.DetallePago.GetById(id);
        }
    }
}