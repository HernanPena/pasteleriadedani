﻿using Business.Interfaces;
using Data.Interfaces;
using Data.Modelos.Principales;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Modelos
{
    public class DireccionBusiness : IDireccionBusiness
    {
        private readonly IUnitOfWork _unitOfWork;

        public DireccionBusiness(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ActionResult<Direccion> AgregarDIreccion(Direccion d)
        {
            return _unitOfWork.Direccion.InsertOrUpdate(d.Id, d);
        }

        public ActionResult<Direccion> DireccionXiD(int id)
        {
            return _unitOfWork.Direccion.GetById(id);
        }

        public ActionResult<Direccion> EliminarDIreccion(int id)
        {
            var d = _unitOfWork.Direccion.GetById(id);
            if (d != null)
            {
                return _unitOfWork.Direccion.Delete(d);
            }
            else
            {
                return new BadRequestObjectResult("No se encuentra direccion");
            }
        }

        public Task<ActionResult<IList<Direccion>>> Listado()
        {
            return _unitOfWork.Direccion.GetAll();
        }
    }
}