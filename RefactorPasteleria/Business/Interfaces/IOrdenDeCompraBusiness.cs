﻿using Data.Modelos.Principales;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Interfaces
{
    public interface IOrdenDeCompraBusiness
    {
        Task<ActionResult<IList<OrdenDeCompra>>> Listado();

        ActionResult<OrdenDeCompra> OrdenCompraXid(int id);

        ActionResult<OrdenDeCompra> AgregarOrdenCompra(OrdenDeCompra p);

        ActionResult<OrdenDeCompra> EliminarORdenCompra(int id);
    }
}