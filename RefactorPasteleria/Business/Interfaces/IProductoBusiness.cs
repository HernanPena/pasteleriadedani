﻿using Data.Modelos.Principales;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Interfaces
{
    public interface IProductoBusiness
    {
        Task<ActionResult<IList<Producto>>> Listado();

        ActionResult<Producto> ProductoXId(int id);

        ActionResult<Producto> ActualizarOAgregarProducto(Producto u);

        ActionResult<Producto> BorrarProducto(int id);
    }
}