﻿using Data.Modelos.Principales;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Interfaces
{
    public interface IDetalleOrdenDeCompraBusiness
    {
        Task<ActionResult<IList<DetalleOrdenDeCompra>>> Listado();

        ActionResult<DetalleOrdenDeCompra> DetalleOrdenDeCompraXid(int id);

        ActionResult<DetalleOrdenDeCompra> AgregarDetalleOrdenDeCompra(DetalleOrdenDeCompra dodc);

        ActionResult<DetalleOrdenDeCompra> EliminarDetalleOrdenDeCompra(int id);
    }
}