﻿using Data.Modelos.Auxiliares;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Interfaces.Auxiliares
{
    public interface IC_Metodo_Pago_Business
    {
        Task<ActionResult<IList<C_Metodo_Pago>>> Listado();

        ActionResult<C_Metodo_Pago> MetodoPagoXid(int id);

        ActionResult<C_Metodo_Pago> AgregarModificarMetodoPago(C_Metodo_Pago metodoPago);

        ActionResult<C_Metodo_Pago> EliminarMetodoPago(int id);
    }
}