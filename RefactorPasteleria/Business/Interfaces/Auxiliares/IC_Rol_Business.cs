﻿using Business.Modelos.Auxiliares;
using Data.Modelos.Auxiliares;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Interfaces.Auxiliares
{
    public interface IC_Rol_Business
    {
        Task<ActionResult<IList<C_Rol>>> Listado();

        ActionResult<C_Rol> RolXid(int id);

        ActionResult<C_Rol> AgregarOmodificarRol(C_Rol Rol);

        ActionResult<C_Rol> EliminarRol(int id);
    }
}