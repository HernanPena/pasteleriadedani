﻿using Data.Modelos.Auxiliares;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Interfaces.Auxiliares
{
    public interface IC_Estado_Orden_Business
    {
        Task<ActionResult<IList<C_Estado_Orden>>> Listado();

        ActionResult<C_Estado_Orden> EstadoOrdenXid(int id);

        ActionResult<C_Estado_Orden> AgregarModificarEstadoOrden(C_Estado_Orden estadoOrden);

        ActionResult<C_Estado_Orden> EliminarEstadoOrden(int id);
    }
}