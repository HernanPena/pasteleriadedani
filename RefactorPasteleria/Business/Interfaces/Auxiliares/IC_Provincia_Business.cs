﻿using Data.Modelos.Auxiliares;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Interfaces.Auxiliares
{
    public interface IC_Provincia_Business
    {
        Task<ActionResult<IList<C_Provincia>>> Listado();

        ActionResult<C_Provincia> ProvinciaXid(int id);

        ActionResult<C_Provincia> AgregarModificarProvincia(C_Provincia provincia);

        ActionResult<C_Provincia> EliminarProvincia(int id);
    }
}