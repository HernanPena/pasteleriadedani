﻿using Data.Modelos.Auxiliares;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Interfaces.Auxiliares
{
    public interface IC_Categoria_Business
    {
        Task<ActionResult<IList<C_Categoria>>> Listado();

        ActionResult<C_Categoria> CategoriaXid(int id);

        ActionResult<C_Categoria> AgregarModificarCategoria(C_Categoria categoria);

        ActionResult<C_Categoria> EliminarCategoria(int id);
    }
}