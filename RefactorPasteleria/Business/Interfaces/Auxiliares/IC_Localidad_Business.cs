﻿using Data.Modelos.Auxiliares;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Interfaces.Auxiliares
{
    public interface IC_Localidad_Business
    {
        Task<ActionResult<IList<C_Localidad>>> Listado();

        ActionResult<C_Localidad> LocalidadXid(int id);

        ActionResult<C_Localidad> AgregarModificarLocalidad(C_Localidad localidad);

        ActionResult<C_Localidad> EliminarLocalidad(int id);
    }
}