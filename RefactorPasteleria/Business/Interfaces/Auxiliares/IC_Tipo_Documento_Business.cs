﻿using Data.Modelos.Auxiliares;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Interfaces.Auxiliares
{
    public interface IC_Tipo_Documento_Business
    {
        Task<ActionResult<IList<C_Tipo_Documento>>> Listado();

        ActionResult<C_Tipo_Documento> TipoDocXid(int id);

        ActionResult<C_Tipo_Documento> AgregarTipoDoc(C_Tipo_Documento TIpoDoc);

        ActionResult<C_Tipo_Documento> EliminarTipoDoc(int id);
    }
}