﻿using Data.Modelos.Principales;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Interfaces
{
    public interface IDireccionBusiness
    {
        Task<ActionResult<IList<Direccion>>> Listado();

        ActionResult<Direccion> DireccionXiD(int id);

        ActionResult<Direccion> AgregarDIreccion(Direccion d);

        ActionResult<Direccion> EliminarDIreccion(int id);
    }
}