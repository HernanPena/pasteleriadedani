﻿using Data.Modelos.Principales;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Interfaces
{
    public interface IUsuarioBusiness
    {
        Task<ActionResult<IList<Usuario>>> Listado();

        ActionResult<Usuario> UsuarioUnico(int id);

        ActionResult<Usuario> BorrarUsuario(int id);

        ActionResult<Usuario> AcctualizarUsuario(Usuario u);
    }
}