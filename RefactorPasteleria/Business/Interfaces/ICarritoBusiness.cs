﻿using Data.Modelos.Principales;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Interfaces
{
    public interface ICarritoBusiness
    {
        Task<ActionResult<IList<Carrito>>> Listado();

        ActionResult<Carrito> CarritoXid(int id);

        ActionResult<Carrito> AgregarCarrito(Carrito dodc);

        ActionResult<Carrito> EliminarCarrito(int id);
    }
}