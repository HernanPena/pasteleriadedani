﻿using Data.Modelos.Principales;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Interfaces
{
    public interface IDetallePagoBusiness
    {
        Task<ActionResult<IList<DetallePago>>> Listado();

        ActionResult<DetallePago> PagoXid(int id);

        ActionResult<DetallePago> AgregarDetallePago(DetallePago dp);

        ActionResult<DetallePago> EliminarDetallePago(int id);
    }
}