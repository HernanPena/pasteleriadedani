﻿using Data.Modelos.Principales;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Interfaces
{
    public interface IPagoBusiness
    {
        Task<ActionResult<IList<Pago>>> Listado();

        ActionResult<Pago> PagoXId(int id);

        ActionResult<Pago> AgregarPago(Pago p);

        ActionResult<Pago> EliminarPago(int id);
    }
}