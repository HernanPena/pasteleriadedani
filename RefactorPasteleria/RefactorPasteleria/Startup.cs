using Business.Interfaces;
using Business.Interfaces.Auxiliares;
using Business.Modelos;
using Business.Modelos.Auxiliares;
using Data;
using Data.Interfaces;
using Data.Interfaces.Auxiliares;
using Data.Repositorios;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace RefactorPasteleria
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDbContext<Contexto>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"), d => d.MigrationsAssembly("RefactorPasteleria")).UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking));
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "RefactorPasteleria", Version = "v1" });
            });

            services.AddScoped(typeof(ICRUD<>), typeof(RepositorioGenerico<>));
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IUsuarioRepo, UsuarioRepo>();
            services.AddScoped<IUsuarioBusiness, UsuarioBusiness>();
            services.AddScoped<IProductoBusiness, ProductoBusiness>();
            services.AddScoped<IProductoRepo, ProductoRepo>();
            services.AddScoped<IPagoBusiness, PagoBusiness>();
            services.AddScoped<IPagoRepo, PagoRepo>();
            services.AddScoped<IOrdenDeCompraRepo, OrdenDeCompraRepo>();
            services.AddScoped<IOrdenDeCompraBusiness, OrdenDeCompraBusiness>();
            services.AddScoped<IDireccionBusiness, DireccionBusiness>();
            services.AddScoped<IDireccionRepo, DireccionRepo>();
            services.AddScoped<IDetallePagoBusiness, DetallePagoBusiness>();
            services.AddScoped<IDetallePagoRepo, DetallePagoRepo>();
            services.AddScoped<IDetalleOrdenDeCompraBusiness, DetalleOrdenDeCompraBusiness>();
            services.AddScoped<IDetalleOrdenDeCompraRepo, DetalleOrdenDeCompraRepo>();
            services.AddScoped<ICarritoBusiness, CarritoBusiness>();
            services.AddScoped<ICarritoRepo, CarritoRepo>();
            services.AddScoped<IC_Tipo_Documento_Business, C_Tipo_Documento_Business>();
            services.AddScoped<IC_Tipo_DocumentoRepo, C_TIpo_DocumentoRepo>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "RefactorPasteleria v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}