﻿using Business.Interfaces.Auxiliares;
using Data.Modelos.Auxiliares;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RefactorPasteleria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoDocumentoController : ControllerBase
    {
        private readonly IC_Tipo_Documento_Business _IC_Tipo_DocumentoBusiness;
        public TipoDocumentoController(IC_Tipo_Documento_Business C_Tipo_DocumentoBusiness)
        {
            _IC_Tipo_DocumentoBusiness = C_Tipo_DocumentoBusiness;
        }
        [HttpGet]
        public Task<ActionResult<IList<C_Tipo_Documento>>> getAll()
        {
            return _IC_Tipo_DocumentoBusiness.Listado();
        }
        [HttpGet("{id}")]
        public ActionResult<C_Tipo_Documento> getId(int id)
        {
            return _IC_Tipo_DocumentoBusiness.TipoDocXid(id);
        }
        [HttpPost]
        public ActionResult<C_Tipo_Documento> AddOrUpdate(C_Tipo_Documento TIpoDoc)
        {
            return _IC_Tipo_DocumentoBusiness.AgregarTipoDoc(TIpoDoc);
        }
        [HttpDelete("{id}")]
        public ActionResult<C_Tipo_Documento> Delete(int id)
        {
            return _IC_Tipo_DocumentoBusiness.EliminarTipoDoc(id);
        }
    




    }
}
