﻿using Business.Interfaces;
using Data.Modelos.Principales;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RefactorPasteleria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private readonly IUsuarioBusiness _usuarioBusiness;

        public UsuarioController(IUsuarioBusiness usuarioBusiness)
        {
            _usuarioBusiness = usuarioBusiness;
        }

        //listado usarios
        [HttpGet]
        public Task<ActionResult<IList<Usuario>>> get()
        {
            return _usuarioBusiness.Listado();
        }

        //usuario id
        [HttpGet("{Id}")]
        public ActionResult<Usuario> get(int id)
        {
            return _usuarioBusiness.UsuarioUnico(id);
        }

        //borrado logico
        [HttpDelete("{Id}")]
        public ActionResult<Usuario> Delete(int id)
        {
            return _usuarioBusiness.BorrarUsuario(id);
        }

        //actualizar usuario
        [HttpPost]
        public ActionResult<Usuario> AddOrUpdate(Usuario u)
        {
            return _usuarioBusiness.AcctualizarUsuario(u);
        }

        //[HttpPost]
        //public ActionResult<Usuario> Post(Usuario usuario)
        //{
        //}
    }
}