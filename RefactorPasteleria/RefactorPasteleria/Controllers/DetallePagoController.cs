﻿using Business.Interfaces;
using Data.Modelos.Principales;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RefactorPasteleria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DetallePagoController : ControllerBase
    {
        private readonly IDetallePagoBusiness _detallePagoBusiness;

        public DetallePagoController(IDetallePagoBusiness detallePagoBusiness)
        {
            _detallePagoBusiness = detallePagoBusiness;
        }

        [HttpGet]
        public Task<ActionResult<IList<DetallePago>>> getAll()
        {
            return _detallePagoBusiness.Listado();
        }

        [HttpGet("{id}")]
        public ActionResult<DetallePago> GetId(int id)
        {
            return _detallePagoBusiness.PagoXid(id);
        }

        [HttpPost]
        public ActionResult<DetallePago> AddOrUpdate(DetallePago dp)
        {
            return _detallePagoBusiness.AgregarDetallePago(dp);
        }

        [HttpDelete("{id}")]
        public ActionResult<DetallePago> Delete(int id)
        {
            return _detallePagoBusiness.EliminarDetallePago(id);
        }
    }
}