﻿using Business.Interfaces.Auxiliares;
using Data.Modelos.Auxiliares;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RefactorPasteleria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RolController : ControllerBase
    {
        private readonly IC_Rol_Business _rolbusiness;
        public RolController(IC_Rol_Business rolbusiness)
        {
            _rolbusiness = rolbusiness;
        }
        [HttpGet]
        public Task<ActionResult<IList<C_Rol>>> GetAll()
        {
            return _rolbusiness.Listado();
        }
        [HttpGet("{id}")]
        public ActionResult<C_Rol> GetID(int id)
        {
            return _rolbusiness.RolXid(id);
        }
        [HttpPost]
        public ActionResult<C_Rol> AgregarOmodificarRol(C_Rol Rol)
        {
            return _rolbusiness.AgregarOmodificarRol(Rol);
        }
        [HttpDelete("{id}")]
        public ActionResult<C_Rol> EliminarRol(int id)
        {
            return _rolbusiness.EliminarRol(id);
        }
        


    }
}
