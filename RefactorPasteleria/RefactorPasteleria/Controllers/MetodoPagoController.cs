﻿using Business.Interfaces.Auxiliares;
using Data.Modelos.Auxiliares;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RefactorPasteleria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MetodoPagoController : ControllerBase
    {
        private readonly IC_Metodo_Pago_Business _metodopagobusiness;
        public MetodoPagoController(IC_Metodo_Pago_Business metodopagobusiness)
        {
            _metodopagobusiness = metodopagobusiness;
        }
        [HttpGet]
        public Task<ActionResult<IList<C_Metodo_Pago>>> GetAll()
        {
            return _metodopagobusiness.Listado();
        }
        [HttpGet("{id}")]
        public ActionResult<C_Metodo_Pago> MetodoPagoXid(int id)
        {
            return _metodopagobusiness.MetodoPagoXid(id);
        }
        [HttpPost]
        public ActionResult<C_Metodo_Pago> AgregarModificarMetodoPago(C_Metodo_Pago metodoPago)
        {
            return _metodopagobusiness.AgregarModificarMetodoPago(metodoPago);
        }
        [HttpDelete("{id}")]
        public ActionResult<C_Metodo_Pago> EliminarMetodoPago(int id)
        {
            return _metodopagobusiness.EliminarMetodoPago(id);
        }
    }
}
