﻿using Business.Interfaces;
using Data.Modelos.Principales;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RefactorPasteleria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DetalleOrdenDeCompraController : ControllerBase
    {
        private readonly IDetalleOrdenDeCompraBusiness _detalleOrdenDeCompraBusiness;
        public DetalleOrdenDeCompraController(IDetalleOrdenDeCompraBusiness detalleOrdenDeCompraBusiness)
        {
            _detalleOrdenDeCompraBusiness = detalleOrdenDeCompraBusiness;
        }
        [HttpGet]
        public Task<ActionResult<IList<DetalleOrdenDeCompra>>> GetAll()
        {
            return _detalleOrdenDeCompraBusiness.Listado();
        }
        [HttpGet("{id}")]
        public ActionResult<DetalleOrdenDeCompra> GetID(int id)
        {
            return _detalleOrdenDeCompraBusiness.DetalleOrdenDeCompraXid(id);
        }
        [HttpDelete("{id}")]
        public ActionResult<DetalleOrdenDeCompra> Delete(int id)
        {
            return _detalleOrdenDeCompraBusiness.EliminarDetalleOrdenDeCompra(id);
        }
        [HttpPost]
        public ActionResult<DetalleOrdenDeCompra> AddOrUpdate(DetalleOrdenDeCompra dodc)
        {
            return _detalleOrdenDeCompraBusiness.AgregarDetalleOrdenDeCompra(dodc);
        }
    }
}
