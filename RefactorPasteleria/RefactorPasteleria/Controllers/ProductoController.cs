﻿using Business.Interfaces;
using Data.Modelos.Principales;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RefactorPasteleria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductoController
    {
        private readonly IProductoBusiness _productoBusiness;

        public ProductoController(IProductoBusiness productoBusiness)
        {
            _productoBusiness = productoBusiness;
        }

        [HttpGet]
        public Task<ActionResult<IList<Producto>>> get()
        {
            return _productoBusiness.Listado();
        }

        [HttpGet("{id}")]
        public ActionResult<Producto> GetID(int id)
        {
            return _productoBusiness.ProductoXId(id);
        }

        [HttpDelete("{id}")]
        public ActionResult<Producto> Delete(int id)
        {
            return _productoBusiness.BorrarProducto(id);
        }

        [HttpPost]
        public ActionResult<Producto> UpdateOrAdd(Producto p)
        {
            return _productoBusiness.ActualizarOAgregarProducto(p);
        }
    }
}