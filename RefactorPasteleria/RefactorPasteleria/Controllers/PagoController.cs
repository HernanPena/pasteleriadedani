﻿using Business.Interfaces;
using Data.Modelos.Principales;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RefactorPasteleria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PagoController : ControllerBase
    {
        private readonly IPagoBusiness _pagoBusiness;

        public PagoController(IPagoBusiness pagoBusiness)
        {
            _pagoBusiness = pagoBusiness;
        }

        [HttpGet]
        public Task<ActionResult<IList<Pago>>> GetAll()
        {
            return _pagoBusiness.Listado();
        }

        [HttpGet("{id}")]
        public ActionResult<Pago> GetByID(int id)
        {
            return _pagoBusiness.PagoXId(id);
        }

        [HttpPost]
        public ActionResult<Pago> AddOrUpdate(Pago p)
        {
            return _pagoBusiness.AgregarPago(p);
        }

        [HttpDelete("{id}")]
        public ActionResult<Pago> Delete(int id)
        {
            return _pagoBusiness.EliminarPago(id);
        }
    }
}