﻿using Business.Interfaces;
using Data.Modelos.Principales;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RefactorPasteleria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DireccionController : ControllerBase
    {
        private readonly IDireccionBusiness _direccionBusiness;

        public DireccionController(IDireccionBusiness direccionBusiness)
        {
            _direccionBusiness = direccionBusiness;
        }

        [HttpGet]
        public Task<ActionResult<IList<Direccion>>> GetAll()
        {
            return _direccionBusiness.Listado();
        }

        [HttpGet("{id}")]
        public ActionResult<Direccion> GetID(int id)
        {
            return _direccionBusiness.DireccionXiD(id);
        }

        [HttpDelete("{id}")]
        public ActionResult<Direccion> Delete(int id)
        {
            return _direccionBusiness.EliminarDIreccion(id);
        }

        [HttpPost]
        public ActionResult<Direccion> AddOrUpdate(Direccion d)
        {
            return _direccionBusiness.AgregarDIreccion(d);
        }
    }
}