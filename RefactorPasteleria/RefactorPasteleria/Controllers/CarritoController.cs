﻿using Business.Interfaces;
using Data.Modelos.Principales;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RefactorPasteleria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarritoController : ControllerBase
    {
        private readonly ICarritoBusiness _carritoBusiness;
        public CarritoController(ICarritoBusiness carritoBusiness)
        {
            _carritoBusiness = carritoBusiness;
        }

        [HttpGet("{id}")]
        public ActionResult<Carrito> CarritoXid(int id)
        {
            return _carritoBusiness.CarritoXid(id);
        }
        [HttpPost]
        public ActionResult<Carrito> AgregarCarrito(Carrito c)
        {
            return _carritoBusiness.AgregarCarrito(c);
        }
        [HttpDelete("{id}")]
        public ActionResult<Carrito> EliminarCarrito(int id)
        {
            return _carritoBusiness.EliminarCarrito(id);
        }
        [HttpGet]
        public Task<ActionResult<IList<Carrito>>> Get()
        {
            return _carritoBusiness.Listado();
        }




    }
}
