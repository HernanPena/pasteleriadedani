﻿using Business.Interfaces;
using Data.Modelos.Principales;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RefactorPasteleria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdenDeCompraController : ControllerBase
    {
        private readonly IOrdenDeCompraBusiness _ordenDeCOmpraBusiness;

        public OrdenDeCompraController(IOrdenDeCompraBusiness ordenDeCompraBusiness)
        {
            _ordenDeCOmpraBusiness = ordenDeCompraBusiness;
        }

        [HttpGet]
        public Task<ActionResult<IList<OrdenDeCompra>>> GetAll()
        {
            return _ordenDeCOmpraBusiness.Listado();
        }

        [HttpGet("{id}")]
        public ActionResult<OrdenDeCompra> GetId(int id)
        {
            return _ordenDeCOmpraBusiness.OrdenCompraXid(id);
        }

        [HttpPost]
        public ActionResult<OrdenDeCompra> AddOrUpdate(OrdenDeCompra OdC)
        {
            return _ordenDeCOmpraBusiness.AgregarOrdenCompra(OdC);
        }

        [HttpDelete("{id}")]
        public ActionResult<OrdenDeCompra> Delete(int id)
        {
            return _ordenDeCOmpraBusiness.EliminarORdenCompra(id);
        }
    }
}