﻿using Business.Interfaces.Auxiliares;
using Data.Modelos.Auxiliares;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RefactorPasteleria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProvinciaController : ControllerBase
    {
        private readonly IC_Provincia_Business _provinciabusiness;
        public ProvinciaController(IC_Provincia_Business provinciabusiness)
        {
            _provinciabusiness = provinciabusiness;
        }
        [HttpGet]
        public Task<ActionResult<IList<C_Provincia>>> GetAll()
        {
            return _provinciabusiness.Listado();
        }
        [HttpGet("{id}")]
        public ActionResult<C_Provincia> getId(int id)
        {
            return _provinciabusiness.ProvinciaXid(id);
        }
        [HttpPost]
        public ActionResult<C_Provincia> AddOrUpdate(C_Provincia provincia)
        {
            return _provinciabusiness.AgregarModificarProvincia(provincia);
        }
        [HttpDelete ("{id}")]
        public ActionResult<C_Provincia> Delete(int id)
        {
            return _provinciabusiness.EliminarProvincia(id);
        }




    }
}
