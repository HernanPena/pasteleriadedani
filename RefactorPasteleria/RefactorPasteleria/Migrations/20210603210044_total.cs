﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RefactorPasteleria.Migrations
{
    public partial class total : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "categorias",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Categoria = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_categorias", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "estadosOrdenes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Estado = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_estadosOrdenes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "metodosPagos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Metodo = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    Detalles_generales = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Detalle_transferencia = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_metodosPagos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "provincias",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Provincia = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_provincias", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "roles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Rol = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tiposDocumentos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Tipo_documento = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tiposDocumentos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "productos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nombre = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    descripcion = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    precio = table.Column<double>(type: "float", nullable: false),
                    id_categoria = table.Column<int>(type: "int", nullable: false),
                    urlImagen = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    stock = table.Column<int>(type: "int", nullable: false),
                    activo = table.Column<bool>(type: "bit", nullable: false),
                    fecha_alta = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_productos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_productos_categorias_id_categoria",
                        column: x => x.id_categoria,
                        principalTable: "categorias",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "localidades",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Localidad = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    Id_provincia = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_localidades", x => x.Id);
                    table.ForeignKey(
                        name: "FK_localidades_provincias_Id_provincia",
                        column: x => x.Id_provincia,
                        principalTable: "provincias",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "usuarios",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nombre = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    apellido = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    email = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: false),
                    id_tipo_documento = table.Column<int>(type: "int", nullable: false),
                    numero_documento = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    telefono = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    password = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    rol_id = table.Column<int>(type: "int", nullable: false),
                    fecha_alta = table.Column<DateTime>(type: "datetime2", nullable: false),
                    activo = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_usuarios", x => x.Id);
                    table.ForeignKey(
                        name: "FK_usuarios_roles_rol_id",
                        column: x => x.rol_id,
                        principalTable: "roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_usuarios_tiposDocumentos_id_tipo_documento",
                        column: x => x.id_tipo_documento,
                        principalTable: "tiposDocumentos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "carritos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    id_usuario = table.Column<int>(type: "int", nullable: false),
                    id_producto = table.Column<int>(type: "int", nullable: false),
                    cantidad = table.Column<int>(type: "int", nullable: false),
                    estado = table.Column<bool>(type: "bit", nullable: false),
                    fecha_alta = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_carritos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_carritos_productos_id_producto",
                        column: x => x.id_producto,
                        principalTable: "productos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_carritos_usuarios_id_usuario",
                        column: x => x.id_usuario,
                        principalTable: "usuarios",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "direcciones",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    id_usuario = table.Column<int>(type: "int", nullable: false),
                    calle = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    numero = table.Column<int>(type: "int", maxLength: 5, nullable: false),
                    id_localidad = table.Column<int>(type: "int", nullable: false),
                    codigo_postal = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_direcciones", x => x.Id);
                    table.ForeignKey(
                        name: "FK_direcciones_localidades_id_localidad",
                        column: x => x.id_localidad,
                        principalTable: "localidades",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_direcciones_usuarios_id_usuario",
                        column: x => x.id_usuario,
                        principalTable: "usuarios",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ordenDeCompras",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    id_usuario = table.Column<int>(type: "int", nullable: false),
                    id_direccion = table.Column<int>(type: "int", nullable: false),
                    id_pago = table.Column<int>(type: "int", nullable: false),
                    total = table.Column<double>(type: "float", nullable: false),
                    id_estado_orden = table.Column<int>(type: "int", nullable: false),
                    fecha_alta = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ordenDeCompras", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ordenDeCompras_direcciones_id_direccion",
                        column: x => x.id_direccion,
                        principalTable: "direcciones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_ordenDeCompras_estadosOrdenes_id_estado_orden",
                        column: x => x.id_estado_orden,
                        principalTable: "estadosOrdenes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_ordenDeCompras_usuarios_id_usuario",
                        column: x => x.id_usuario,
                        principalTable: "usuarios",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    
                });

            migrationBuilder.CreateTable(
                name: "detalleOrdenDeCompras",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    id_orden_compra = table.Column<int>(type: "int", nullable: false),
                    id_producto = table.Column<int>(type: "int", nullable: false),
                    cantidad = table.Column<int>(type: "int", nullable: false),
                    precioUnitario = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_detalleOrdenDeCompras", x => x.Id);
                    table.ForeignKey(
                        name: "FK_detalleOrdenDeCompras_ordenDeCompras_id_orden_compra",
                        column: x => x.id_orden_compra,
                        principalTable: "ordenDeCompras",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_detalleOrdenDeCompras_productos_id_producto",
                        column: x => x.id_producto,
                        principalTable: "productos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "pagos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    id_orden_compra = table.Column<int>(type: "int", nullable: false),
                    fecha = table.Column<DateTime>(type: "datetime2", nullable: false),
                    total = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_pagos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_pagos_ordenDeCompras_id_orden_compra",
                        column: x => x.id_orden_compra,
                        principalTable: "ordenDeCompras",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "detallePagos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    id_pago = table.Column<int>(type: "int", nullable: false),
                    id_metodo_pago = table.Column<int>(type: "int", nullable: false),
                    pago_realizado = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_detallePagos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_detallePagos_metodosPagos_id_metodo_pago",
                        column: x => x.id_metodo_pago,
                        principalTable: "metodosPagos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_detallePagos_pagos_id_pago",
                        column: x => x.id_pago,
                        principalTable: "pagos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_carritos_id_producto",
                table: "carritos",
                column: "id_producto");

            migrationBuilder.CreateIndex(
                name: "IX_carritos_id_usuario",
                table: "carritos",
                column: "id_usuario");

            migrationBuilder.CreateIndex(
                name: "IX_detalleOrdenDeCompras_id_orden_compra",
                table: "detalleOrdenDeCompras",
                column: "id_orden_compra");

            migrationBuilder.CreateIndex(
                name: "IX_detalleOrdenDeCompras_id_producto",
                table: "detalleOrdenDeCompras",
                column: "id_producto");

            migrationBuilder.CreateIndex(
                name: "IX_detallePagos_id_metodo_pago",
                table: "detallePagos",
                column: "id_metodo_pago");

            migrationBuilder.CreateIndex(
                name: "IX_detallePagos_id_pago",
                table: "detallePagos",
                column: "id_pago");

            migrationBuilder.CreateIndex(
                name: "IX_direcciones_id_localidad",
                table: "direcciones",
                column: "id_localidad");

            migrationBuilder.CreateIndex(
                name: "IX_direcciones_id_usuario",
                table: "direcciones",
                column: "id_usuario");

            migrationBuilder.CreateIndex(
                name: "IX_localidades_Id_provincia",
                table: "localidades",
                column: "Id_provincia");

            migrationBuilder.CreateIndex(
                name: "IX_ordenDeCompras_id_direccion",
                table: "ordenDeCompras",
                column: "id_direccion");

            migrationBuilder.CreateIndex(
                name: "IX_ordenDeCompras_id_estado_orden",
                table: "ordenDeCompras",
                column: "id_estado_orden");

            migrationBuilder.CreateIndex(
                name: "IX_ordenDeCompras_id_pago",
                table: "ordenDeCompras",
                column: "id_pago");

            migrationBuilder.CreateIndex(
                name: "IX_ordenDeCompras_id_usuario",
                table: "ordenDeCompras",
                column: "id_usuario");

            migrationBuilder.CreateIndex(
                name: "IX_pagos_id_orden_compra",
                table: "pagos",
                column: "id_orden_compra");

            migrationBuilder.CreateIndex(
                name: "IX_productos_id_categoria",
                table: "productos",
                column: "id_categoria");

            migrationBuilder.CreateIndex(
                name: "IX_usuarios_id_tipo_documento",
                table: "usuarios",
                column: "id_tipo_documento");

            migrationBuilder.CreateIndex(
                name: "IX_usuarios_rol_id",
                table: "usuarios",
                column: "rol_id");

            migrationBuilder.AddForeignKey(
                name: "FK_ordenDeCompras_pagos_id_pago",
                table: "ordenDeCompras",
                column: "id_pago",
                principalTable: "pagos",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_direcciones_usuarios_id_usuario",
                table: "direcciones");

            migrationBuilder.DropForeignKey(
                name: "FK_ordenDeCompras_usuarios_id_usuario",
                table: "ordenDeCompras");

            migrationBuilder.DropForeignKey(
                name: "FK_pagos_ordenDeCompras_id_orden_compra",
                table: "pagos");

            migrationBuilder.DropTable(
                name: "carritos");

            migrationBuilder.DropTable(
                name: "detalleOrdenDeCompras");

            migrationBuilder.DropTable(
                name: "detallePagos");

            migrationBuilder.DropTable(
                name: "productos");

            migrationBuilder.DropTable(
                name: "metodosPagos");

            migrationBuilder.DropTable(
                name: "categorias");

            migrationBuilder.DropTable(
                name: "usuarios");

            migrationBuilder.DropTable(
                name: "roles");

            migrationBuilder.DropTable(
                name: "tiposDocumentos");

            migrationBuilder.DropTable(
                name: "ordenDeCompras");

            migrationBuilder.DropTable(
                name: "direcciones");

            migrationBuilder.DropTable(
                name: "estadosOrdenes");

            migrationBuilder.DropTable(
                name: "pagos");

            migrationBuilder.DropTable(
                name: "localidades");

            migrationBuilder.DropTable(
                name: "provincias");
        }
    }
}
