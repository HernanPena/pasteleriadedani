﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RefactorPasteleria.Migrations
{
    public partial class fix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ordenDeCompras_pagos_id_pago",
                table: "ordenDeCompras");

            migrationBuilder.DropIndex(
                name: "IX_ordenDeCompras_id_pago",
                table: "ordenDeCompras");

            migrationBuilder.DropColumn(
                name: "id_pago",
                table: "ordenDeCompras");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "id_pago",
                table: "ordenDeCompras",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ordenDeCompras_id_pago",
                table: "ordenDeCompras",
                column: "id_pago");

            migrationBuilder.AddForeignKey(
                name: "FK_ordenDeCompras_pagos_id_pago",
                table: "ordenDeCompras",
                column: "id_pago",
                principalTable: "pagos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
